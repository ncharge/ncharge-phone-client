# Instructions on How to Use the Application

Since we've started adding native modules from Finix and Plaid into the application,
we're creating this README to explain how to get the application fully operational

### Install:
```
yarn
```
Add Plaid by running in the top-level directory:
```
yarn add linkkit
```
Link Plaid to the name react-native-plaid-link
```
cd linkkit
yarn link
```


#### Run:
```
yarn start
```

#### Test:
Integration tests:
```
detox test # currently not being used
```
Component and Unit Tests:
```
yarn test
```

