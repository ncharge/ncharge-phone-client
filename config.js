export const MAX_AMOUNT = 50000;
export default config = {
	crbApiGateway: {
		URL: 'crossriver.sandbox-payments-api.com',
		APP_ID: 'APjpvbuX5xDWtKYf8nM6w7z7'
	},
	nchargeApiGateway: {
		REGION: "us-west-2",
		URL: "https://dev-api.ncharge.io"
	},
	cognito: {
		REGION: "us-west-2",
		USER_POOL_ID: "us-west-2_uQxADfJ9l",
		APP_CLIENT_ID: "6ih9peknmobno8arvjglhvlbo6",
		IDENTITY_POOL_ID: "us-west-2:d5dacfb1-7e75-4138-b0e2-84e84096c9c6"
	},
};
