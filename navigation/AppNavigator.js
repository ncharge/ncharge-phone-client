import React from 'react';
import {
	Button,
} from 'react-native';
import { createStackNavigator, createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import AuthScreen from '../screens/AuthScreen';
import PostSignUpScreen from '../screens/PostSignUpScreen';

const AuthStack = createStackNavigator({
	Auth: {
		screen: AuthScreen,
		navigationOptions: () => ({
			header: null,
		}),
	},
	PostSignUp: {
		screen: PostSignUpScreen,
		navigationOptions: () => ({
			header: null,
		}),
	}
});

export default createSwitchNavigator(
	// You could add another route here for authentication.
	// Read more at https://reactnavigation.org/docs/en/auth-flow.html
	{
		Auth: AuthStack,
		Main: MainTabNavigator,
	},
	{
		initialRouteName: 'Auth',
	}
);
