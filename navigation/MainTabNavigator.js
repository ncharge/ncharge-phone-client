import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import ContractDetailScreen from '../screens/ContractDetail';
import PayDirectScreen from '../screens/PayDirect';
import SettingsScreen from '../screens/SettingsScreen';
import QuickBooksScreen from '../screens/QuickBooksScreen';
import PaymentMethodsScreen from '../screens/PaymentMethodsScreen';

import nColors from '../common/nColors';

const HomeStack = createStackNavigator({
	Home: HomeScreen,
	ContractDetail: ContractDetailScreen,
	PayDirect: PayDirectScreen,
});

HomeStack.navigationOptions = {
	tabBarLabel: 'Home',
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={
				Platform.OS === 'ios'
					? `ios-home`
					: 'md-home'
			}
		/>
	),
};

const AddStack= createStackNavigator({
	PayDirect: PayDirectScreen,
});

AddStack.navigationOptions = {
	tabBarLabel: 'Add Contract',
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={Platform.OS === 'ios' ? 'ios-add-circle' : 'md-add-circle'}
		/>
	),
};

const QuickBooksStack = createStackNavigator({
	QuickBooks: QuickBooksScreen,
	PayDirect: PayDirectScreen,
});

QuickBooksStack.navigationOptions = {
	tabBarLabel: 'Integrations',
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={Platform.OS === 'ios' ? 'ios-book' : 'md-book'}
		/>
	),
};

const SettingsStack = createStackNavigator({
	Settings: SettingsScreen,
	PaymentMethods: PaymentMethodsScreen,
});

SettingsStack.navigationOptions = {
	tabBarLabel: 'Settings',
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
		/>
	),
};

export default createBottomTabNavigator({
	HomeStack,
	AddStack,
	QuickBooksStack,
	SettingsStack,
});
