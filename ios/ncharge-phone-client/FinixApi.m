//
//  FinixApi.m
//  ncharge-phone-client
//
//  Created by Roshan on 1/16/19.
//  Copyright © 2019 650 Industries, Inc. All rights reserved.
//

#import "React/RCTBridgeModule.h"

// TODO: replace back with @interface RCT_EXTERN_MODULE(FinixApi, NSObject) after upgrading to an Expo release that includes React Native v0.59.3
RCT_EXTERN void RCTRegisterModule(Class);
@interface FinixApi : NSObject
@end
@interface FinixApi (RCTExternModule) <RCTBridgeModule>
@end

@implementation FinixApi (RCTExternModule)

+ (NSString *)moduleName { return @"FinixApi"; }
__attribute__((constructor)) static void initialize_FinixApi() {
    RCTRegisterModule([FinixApi class]);
}

RCT_EXTERN_METHOD(initTokenizer:(NSString *)endpointUrl appId:(NSString *)appId)
RCT_EXTERN_METHOD(createToken:(NSString *)cardNumber expirationYear:(NSString *)expirationYear expirationMonth:(NSString *)expirationMonth resolve:(RCTPromiseResolveBlock)resolve reject:(RCTPromiseRejectBlock)reject)
@end
