//
//  FinixApi.swift
//  ncharge-phone-client
//
//  Created by Roshan on 1/16/19.
//  Copyright © 2019 650 Industries, Inc. All rights reserved.
//

import Foundation
import PaymentsSDK

@objc(FinixApi)
class FinixApi: NSObject {
    var tokenizer: Tokenizer? = nil

    @objc
    static func requiresMainQueueSetup() -> Bool {
        return false
    }

    @objc(initTokenizer:appId:)
    func initTokenizer(_ endpointUrl: String, _ appId: String) {
        tokenizer = Tokenizer(host: endpointUrl, applicationId: appId)
        print("endpointUrl: \(endpointUrl), appId: \(appId)")
    }
    
    enum TokenizerError: Error {
        case nullOrEmptyParameters
        case serializationFailure
        case initializationFailure
        case parseFailure
    }

    func isEmpty(_ string: String?) -> Bool {
        return (string ?? "").isEmpty
    }
    
    @objc(createToken:expirationYear:expirationMonth:resolve:reject:)
    func createToken(cardNumber: String?,
                     expirationYear: String?,
                     expirationMonth: String?,
                     resolve: @escaping RCTPromiseResolveBlock,
                     reject: @escaping RCTPromiseRejectBlock) {
        if (isEmpty(cardNumber) || isEmpty(expirationYear) || isEmpty(expirationMonth)) {
            reject("E_TOKENIZER", "Empty or null fields were provided", TokenizerError.nullOrEmptyParameters)
            return
        }
        let expirationMonthInt = Int(expirationMonth!)
        let expirationYearInt = Int(expirationYear!)
        if (expirationMonthInt == nil || expirationYearInt == nil) {
            reject("E_TOKENIZER", "Parsing error occurred for expirationYear and/or expirationMonth", TokenizerError.parseFailure)
            return
        }
        guard tokenizer?.createToken(cardNumber: cardNumber!, paymentType: PaymentType.PAYMENT_CARD, expirationMonth: expirationMonthInt!, expirationYear: expirationYearInt!, completion: {
            (token, error) in
            guard let token = token else {
                print(error!.localizedDescription)
                reject("E_FINIX", error!.localizedDescription, error)
                return
            }
            print("token id: \(token.id)")
            print("token fingerprint: \(token.fingerprint)")
            let jsonEncoder = JSONEncoder()
            if #available(iOS 10.0, *) {
                print("using built-in ISO-8601 dateEncodingStrategy")
                jsonEncoder.dateEncodingStrategy = .iso8601
            } else {
                print("using custom ISO-8601 dateEncodingStrategy")
                jsonEncoder.dateEncodingStrategy = .formatted({
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
                    formatter.calendar = Calendar(identifier: .iso8601)
                    formatter.timeZone = TimeZone(secondsFromGMT: 0)
                    formatter.locale = Locale(identifier: "en_US_POSIX")
                    return formatter
                }())
            }
            if let data = try? jsonEncoder.encode(token) {
                let jsonString = String(data: data, encoding: .utf8)
                print(jsonString!)
                resolve(jsonString)
            } else {
                reject("E_TOKENIZER", "JSON serialization failed", TokenizerError.serializationFailure)
            }
        }) != nil else {
            reject("E_TOKENIZER", "Tokenizer not initalized", TokenizerError.initializationFailure)
            return
        }
    }
}
