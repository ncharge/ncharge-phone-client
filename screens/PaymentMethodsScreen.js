import React from 'react';
import {
	ActivityIndicator,
	Modal,
	Platform,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from 'react-native';
import nColors from '../common/nColors';
import NButton from '../common/nButton';

import {withGlobalContext} from '../common/GlobalContext';

import PlaidAuthenticator from '../components/PlaidAuthenticator';
import ChargeBankAccountModal from '../components/ChargeBankAccountModal';

import { CardIOModule, CardIOUtilities } from 'react-native-awesome-card-io';

import { API } from 'aws-amplify';

class PaymentMethodsScreen extends React.Component {
	static navigationOptions = {
		title: 'Payment Methods',
	}

	constructor(props) {
		super(props);

		this.state = {
			loadingPlaidAccount: false,
			isPlaidModalVisible: false,
			isChargeModalVisible: false,
		}
	}

	logInfo = (...args) => {
		console.log(`${this.constructor.displayName}:`, ...args);
	}

	componentWillMount = () => {
		if(Platform.OS === 'ios') {
			CardIOUtilities.preload();
		}
	}

	async componentDidMount() {
		if (this.props.global.plaidAccount === null) {
			this.setState({ loadingPlaidAccount: true });
			await this.props.global.refreshPlaidAccount();
			this.setState({ loadingPlaidAccount: false });
		}
	}

	setPlaidModalVisible = () => {
		this.setState({ isPlaidModalVisible: true });
	}

	scanCard = () => {
		CardIOModule
			.scanCard()
			.then(card => {
				this.props.global.updateCardData([
					...this.props.global.cards,
					{
						cardNumber: card.cardNumber,
						cvv: card.cvv,
						expMonth: card.expiryMonth.toString().padStart(2, '0'),
						expYear: card.expiryYear.toString(),
						cardType: card.cardType,
					}
				]);
			})
			.catch(() => {
				// the user cancelled
			})
	}

	onMessage = (data) => {
		const status = data.action.substr(data.action.lastIndexOf(':') + 1).toUpperCase()
		this.props.global.logInfo("debug Data:", data)
		this.logInfo("Status", status)
		if (status === 'CONNECTED' || status === 'EXIT'){
			if (data.metadata != null) {
				const public_token = data.metadata.public_token;
				const account_id = data.metadata.account_id;
				if (public_token != null && public_token != ''
					&& account_id != null && account_id != '') {
					this.logInfo('Sending Plaid Link public_token and account_id for access_token creation');
					this.props.global.updatePlaidAccount(
						account_id,
						data.metadata.account.name,
						data.metadata.account.mask,
					);

					try {
						this.createPlaidAccessToken(public_token, account_id);
					} catch(e) {
						this.logInfo(e);
					}
				} else {
					this.logInfo('Empty public_token/account_id provided, skipping access_token creation');
				}
			}
			this.setState({isPlaidModalVisible: false})
		}
	}

	createPlaidAccessToken = async (public_token, account_id) => {
		try {
			const response = await this.postCreatePlaidAccessToken(public_token, account_id);
			this.logInfo('Received response', response);
		} catch (e) {
			this.logInfo('createPlaidAccessToken failed', e);
		}
	}

	postCreatePlaidAccessToken = (public_token, account_id) => {
		return API.post("ncharge", "/admin/create-plaid-access-token", {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: {
				"public_token": public_token,
				"account_id": account_id,
			}
		});
	}

	showChargeModal = () => {
		this.setState({ isChargeModalVisible: true });
	}
	updateAmount = (amount) => {
		this.setState({amount})
	}

	dismissChargeModal = () => {
		this.setState({ isChargeModalVisible: false });
	}

	renderCards(cards) {
		if (cards.length > 0) {
			return <View>
				{
					cards.map((card, index) => (
						<View key={index} style={styles.account}>
							<View>
								<Text>{card.cardType}-{card.cardNumber.slice(-4)}</Text>
								<Text>Expires {card.expMonth}/{card.expYear}</Text>
							</View>
						</View>
					))
				}
			</View>;
		}

		return <Text>No linked cards</Text>;
	}

	renderSelectedAccount(plaidAccount) {
		if (this.state.loadingPlaidAccount) {
			
			return <View style={{ alignItems: 'flex-start' }}>
				<ActivityIndicator />
			</View>
		}

		if (plaidAccount !== null) {
			return <View>
				<View style={styles.account}>
					<Text>{plaidAccount.name}-{plaidAccount.mask}</Text>
					<NButton
						theme="bordered"
						type="small"
						onPress={this.showChargeModal}
						caption='Withdraw funds'
					/>
				</View>
			</View>;
		}

		return <Text>No saved bank accounts</Text>;
	}

	render() {
		const { cards, plaidAccount } = this.props.global;
		return (
			<View style={styles.container}>
				<Modal
					animationType='slide'
					transparent={false}
					visible={this.state.isPlaidModalVisible}
				>
					<PlaidAuthenticator
						onMessage={this.onMessage}
						publicKey="f477603af256e0521afc62afc3c4d8"
						env="sandbox"
						product="auth,transactions"
						clientName="nCharge"
						selectAccount={true}
					/>
				</Modal>
				<ChargeBankAccountModal
					account_id={plaidAccount !== null ? plaidAccount.id : null}
					visible={this.state.isChargeModalVisible}
					onDismiss={this.dismissChargeModal}
				/>
				<View style={styles.accountsContainer}>
					<Text style={styles.subtitle}>Cards</Text>
					{ this.renderCards(cards) }
					<Text style={styles.subtitle}>Bank Accounts</Text>
					{ this.renderSelectedAccount(plaidAccount) }
				</View>
				<View>
					<NButton
						style={styles.button}
						type="small"
						theme="bordered"
						onPress={this.scanCard}
						caption='Add a credit card'
					/>
					<NButton
						style={styles.button}
						type="small"
						theme="bordered"
						onPress={this.setPlaidModalVisible}
						caption='Add a bank account'
					/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'space-between',
		backgroundColor: nColors.white,
		paddingVertical: 15,
		paddingHorizontal: 10,
	},
	button: {
		marginTop: 5,
	},
	subtitle: {
		fontWeight: 'bold',
	},
	accountsContainer: {
	},
	account: {
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: nColors.coolGray,
		paddingHorizontal: 10,
		paddingVertical: 10,
		marginBottom: 3,
		shadowColor: nColors.black,
		shadowOffset: { width: 0, height: 1},
		shadowRadius: 5,
		shadowOpacity: 0.2,
		backgroundColor: nColors.white,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
});

export default withGlobalContext(PaymentMethodsScreen);

