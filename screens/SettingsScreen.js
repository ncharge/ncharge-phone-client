import React from 'react';
import { Auth } from 'aws-amplify';
import {
	StyleSheet,
	Text,
	Button,
	TouchableOpacity,
	View,
} from 'react-native';
import nColors from '../common/nColors';
import NButton from '../common/nButton';

import TabView from '../components/TabView';

import {withGlobalContext} from '../common/GlobalContext';

class SettingsScreen extends React.Component {
	static navigationOptions = {
		title: 'Settings',
		headerStyle: {
			backgroundColor: nColors.green
		}
	}

	_handleLogout = async () => {
		await Auth.signOut()
			.then(() => {
				this.props.global.clearContext();
				this.props.navigation.navigate('Auth');
			})
			.catch(err => {console.log('err: ', err)});
	}

	render() {
		return (
			<View style={styles.container}>
				<NButton
					theme="settings"
					type="settings"
					onPress={() => this.props.navigation.navigate('PaymentMethods')}
					caption='Add/Manage Payment Methods'
				/>
				<NButton
					theme="settings"
					type="settings"
					onPress={() => this.props.navigation.navigate('PaymentMethods')}
					caption='Edit Profile'
				/>
				<NButton
					theme="settings"
					type="settings"
					onPress={() => this.props.navigation.navigate('PaymentMethods')}
					caption='Change Password'
				/>
				<View style={{height: 30}}/>
				<NButton
					theme="settings"
					type="settings"
					icon={require("../common/img/logo.png")}
					onPress={this._handleLogout}
					caption='Logout'
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: "#eee",
	},
});

export default withGlobalContext(SettingsScreen);

