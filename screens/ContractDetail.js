import React, { Component } from 'react';
import {
	TextInput,
	Image,
	Button,
	Platform,
	ScrollView,
	FlatList,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
	StatusBar,
	AlertIOS,
	ActivityIndicator,
} from 'react-native';
import nColors from '../common/nColors'
import NButton from '../common/nButton'
import { validateAmount } from '../common/util'
import { withGlobalContext } from '../common/GlobalContext'

import TxnList from '../components/TxnList'
import AmountTextField from '../components/AmountTextField'

import { API } from 'aws-amplify';

class ContractDetail extends Component {
	static navigationOptions = {
		title: 'Contract Detail',
	}

	constructor (props) {
		super(props);

		this.state = {
			first_name : '',
			user_id : '',
			transactions: [],
			cards: [],
			amount: '',
			isLoading: false,
			submit: false,
		}
	}

	componentWillMount = () => {
		this._navListener = this.props.navigation.addListener('didFocus', () => {
			StatusBar.setBarStyle('dark-content');
		});
	}

	componentDidMount = async () => {
		try {
			const first_name = this.props.navigation.getParam('nickname', '')
			const user_id = this.props.navigation.getParam('user_id', '')
			const transactions = await this.getAdminTransactions(user_id);
			const cards = await this.getAdminCards(user_id);
			this.setState({first_name, user_id, transactions, cards});
		} catch (e) {
			console.log('Caught: ', e);
			console.log('error data:', JSON.stringify(e.response.data));
			AlertIOS.alert(
				'Error',
				e.response.data.errorMessage,
				[
					{
						text: 'OK',
						onPress: () => this.props.navigation.pop(),
					},
				],
			);
		}
	}

	componentWillUnmount = () => {
		this._navListener.remove();
	}

	_confirmPayee = (user_id) => {
		return API.put("ncharge", "/admin/confirm", { 
			body: {
				"payee_user_id": user_id
			}
		});
	}

	_deletePayee = (user_id) => {
		return API.delete("ncharge", "/relationship", { 
			body: {
				"payee_user_id": user_id
			}
		});
	}

	_nCharge = (card) => {
		console.log('amount: ', this.state.amount);
		const request = { 
			body: {
				"payee_user_id": this.state.user_id,
				"card_id": card.id,
				"amount": parseFloat(this.state.amount+'00'),
			}
		};
		console.log('request: ', request);
		return API.post("ncharge", "/ncharge", { 
			body: {
				"payee_user_id": this.state.user_id,
				"card_id": card.id,
				"amount": parseFloat(this.state.amount+'00'),
			}
		});
	}

	processCharge = () => {
		if (this.state.amount < 1) {
			alert("Please enter an amount between $1-$50,000")
			return;
		}
		__handleCharge = () => {
			this.setState({ isLoading: true });
			this._nCharge(this.state.cards[0])
				.then((txn) => {
					// TODO: send balance back in response and update directly to avoid extra API call?
					this.props.global.refreshBalance();
					this.setState({
						transactions: this.state.transactions.concat(txn),
						isLoading: false
					});
				})
				.catch((e) => {
					console.log('Caught', e)
					console.log('error data:', JSON.stringify(e.response.data));
					AlertIOS.alert(
						'Error',
						e.response.data.errorMessage,
						null
					);
					this.setState({ isLoading: false });
				});
		}
		try {
			AlertIOS.alert(
				'nCharge!',
				`Now paying $${this.state.amount} to ${this.state.first_name}`,
				[
					{
						text: 'Cancel',
						onPress: () => {},
						style: 'cancel',
					},
					{
						text: 'Confirm',
						onPress: () => __handleCharge(),
					},
				],
			);
		} catch (e) {
			alert(e);
			alert(e.message);
		}
	}

	handleDelete = async (user_id) => {
		try {
			const status = await this._deletePayee(user_id);
			if (__DEV__)
				console.log("Deletion status status", status);
		} catch (e) {
			alert("Deletion failed!", e);
			console.log(e.message);
		}

	}

	handleConfirmation = async (user_id) => {
		try {
			const status = await this._confirmPayee(user_id);
			if (__DEV__)
				console.log("Confirmation status", status);
		} catch (e) {
			alert("Confirmation failed!", e);
			console.log(e.message);
		}

	}

	_disableSubmit = (submit) => {
		this.setState({submit})
	}
	_setAmount = (amount) => {
		this.setState({amount})
	}
	getAdminTransactions(user_id) {
		return API.get("ncharge", "/admin/transactions?payee_user_id=" + user_id);
	}

	getAdminCards(user_id) {
		return API.get("ncharge", "/payee/cards", {
			queryStringParameters: {
				"payee_user_id": user_id
			}
		});
	}

	dateConvert(datetime) {
		let date = new Date(datetime);
		return date.toLocaleString('en-US')
	}

	render() {
		const {navigation} = this.props
		const pending = navigation.getParam('pending', false);
		if(pending)
			return (
				<View style={styles.container}>
				<StatusBar
					backgroundColor="blue"
					barStyle="light-content"
				/>
					<Text style={styles.description}>Confirm {this.state.user_id}?</Text>
					<View style={styles.buttonContainer}>
						<NButton 
							caption="Confirm"
							type="small"
							theme="fb"
							onPress={this.handleConfirmation}
						/>
						<NButton
							onPress={this.handleDelete}
							caption="Delete"
							type="small"
						/>
					</View>
				</View>
			);
		else if (this.state.user_id) {
			return (
				<ScrollView
					style={styles.container}
					scrollEnabled={false}
					keyboardShouldPersistTaps="never"
				>
					<AmountTextField
						disableSubmit={this._disableSubmit}
						setAmount={this._setAmount}
					/>
					<NButton caption="nCharge!"
						style={styles.ncharge}
						icon={require("../common/img/logo.png")}
						type="large"
						theme={this.state.submit ? 'disabled' : "blue"}
						onPress={this.processCharge}
						isLoading={this.state.isLoading}
					/>
					<TxnList key="transactionList" transactions={this.state.transactions} title="Transactions"/>
				</ScrollView>
			);
		}
		else {
			return (
				<View style={styles.loadingContainer}>
					<ActivityIndicator />
					<Text>Retrieving Contract Details...</Text>
				</View>
			);
		}
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: nColors.white,
		padding:4,
	},
	description: {
		fontSize: 17,
		lineHeight: 24, 
		textAlign: 'center',
	},
	loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		textAlign: 'center',
		backgroundColor: nColors.white,
		padding:4,
	},
	buttonContainer: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
	},
	ncharge: {
		margin: 5,
	},
	transaction: {
		borderStyle: 'solid',
		borderRadius: 10,
		borderWidth: 1,
		borderColor: nColors.coolGray,
		textAlign: 'right',
		height: 75,
		paddingHorizontal: 10,
		paddingVertical: 10,
		marginBottom: 3,
		backgroundColor: nColors.white,
	},
	textInput: {
		borderWidth: 0.5,
		borderColor: nColors.coolGray,
		backgroundColor: nColors.white,
		height: 60,
		fontSize:20,
		marginBottom: 10,
		paddingLeft: 10,
		borderRadius: 10,
	}
});

export default withGlobalContext(ContractDetail);

