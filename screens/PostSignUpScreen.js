import React from 'react';
import {
	Button,
	Image,
	View,
} from 'react-native';
import { Feather } from '@expo/vector-icons'
import NButton from '../common/nButton'
import nColors from '../common/nColors'
import Onboarding from 'react-native-onboarding-swiper';

const PostSignUpScreen = (props) => 
	<Onboarding
		DotComponent={() => null}
		onSkip={() => props.navigation.navigate('Main')}
		onDone={() => props.navigation.navigate('Main')}
		pages={[
			{
				backgroundColor: '#1B998B',
				image: <Feather name="link" color={nColors.bianca} size={120} />,
				title: 'You\'re almost there! Link a bank account to begin nCharging.',
				subtitle: <NButton
					theme='fb'
					onPress={() => props.navigation.navigate('PaymentMethods')}
					caption='Add a payment method'
				/>,
			},
		]}
	/>

export default PostSignUpScreen;

