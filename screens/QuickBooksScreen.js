import React from 'react';
import {
	ActivityIndicator,
	Dimensions,
	FlatList,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
	Button,
} from 'react-native';
import { Base64 } from 'js-base64';
import { AuthSession, Linking, WebBrowser } from 'expo';
import nColors from '../common/nColors'
import NButton from '../common/nButton'
import ContractList from '../components/ContractList';
import TabView from '../components/TabView';

const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');

export default class QuickBooksScreen extends React.Component {
	static navigationOptions = {
		title: 'Integrations',
		headerStyle: {
			backgroundColor: nColors.green,
		}
	};

	constructor (props) {
		super(props);

		// NOTE: Expo doesn't recommend saving tokens in application but whatever
		this.state = {
			apiBaseUrl: 'https://sandbox-quickbooks.api.intuit.com',
			clientId: 'Q0J1oQush7uUx5qOKk1K3G3Jw4daw1dl2n2T4Os9z0MvgdUCap',
			clientSecret: 'ZMoJZv3NnqWdihWV7rCjlbsZDcTB60ehwMPl8MV7',
			signedIn: false,
			realmId: '',
			accessToken: '',
			refreshToken: '',
			companyName: '',
			contractors: [],
		}
	}

	handleSignIn = async () => {
		const redirectUrl = AuthSession.getRedirectUrl();
		const url = `https://appcenter.intuit.com/connect/oauth2?client_id=${this.state.clientId}&scope=com.intuit.quickbooks.accounting%20com.intuit.quickbooks.payment&response_type=code&state=testState&redirect_uri=${redirectUrl}`;
		console.log('Opening ' + url + ' in browser');
		const result = await AuthSession.startAsync({
			authUrl: url
		});
		console.log(result);
		if (result.params && result.params.code && result.params.realmId) {
			let tokenResponse = await this.exchangeCodeForTokens(result.params.code, redirectUrl);
			this.setState({
				signedIn: true,
				realmId: result.params.realmId,
				accessToken: tokenResponse.access_token,
				refreshToken: tokenResponse.refresh_token,
			});
			const companyInfo = await this.getCompanyInfo();
			const vendorResponse = await this.queryVendors('select * from vendor');
			this.setState({
				companyName: companyInfo.CompanyName,
				contractors: vendorResponse.Vendor ? vendorResponse.Vendor.filter(v => v.Vendor1099) : [],
			});
		} else {
			console.log('cancelled');
		}
	}

	exchangeCodeForTokens(authCode, redirectUrl) {
		let body = `grant_type=authorization_code&code=${authCode}&redirect_uri=${redirectUrl}`;
		return this.getTokens(body);
	}

	refreshTokens() {
		let body = `grant_type=refresh_token&refresh_token=${this.state.refreshToken}`;
		return this.getTokens(body);
	}

	getTokens(bodyContent) {
		return fetch('https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/x-www-form-urlencoded',
				'Authorization': 'Basic ' + Base64.encode(this.state.clientId + ":" + this.state.clientSecret)
			},
			body: bodyContent,
		})
			.then(response => response.json())
			.catch(error => {
				console.log(error);
			});
	}

	revokeAccessToken() {
		return fetch('https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Basic ' + Base64.encode(this.state.clientId + ":" + this.state.clientSecret)
			},
			body: JSON.stringify({
				'token': this.state.accessToken
			}),
		})
			.then(response => response.ok)
			.catch(error => {
				console.log(error);
			});
	}

	handleSignOut = () => {
		// clear all codes/tokens, is this how you're supposed to do it??
		console.log(this.revokeAccessToken());
		this.setState({
			signedIn: false,
			accessToken: '',
			refreshToken: '',
			contractors: [],
		});
	}

	queryVendors = (query) => {
		const url = `${this.state.apiBaseUrl}/v3/company/${this.state.realmId}/query?query=${encodeURI(query)}`;
		return fetch(url, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Authorization': 'Bearer ' + this.state.accessToken
			},
		})
			.then(response => response.json())
			.then(responseJson => responseJson.QueryResponse)
			.catch(error => {
				console.log(error);
			});
	}

	getCompanyInfo = async () => {
		const url = `${this.state.apiBaseUrl}/v3/company/${this.state.realmId}/companyinfo/${this.state.realmId}`;
		return fetch(url, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Authorization': 'Bearer ' + this.state.accessToken
			},
		})
			.then(response => response.json())
			.then(responseJson => responseJson.CompanyInfo)
			.catch(error => {
				console.log(error);
			});
	}

	renderSignedIn() {
		if (this.state.companyName.length > 0) {
			return (
				<View style={styles.signedInContainer}>
					<Text>Currently logged into {this.state.companyName}</Text>
					{
						this.state.contractors.length > 0 ? (
							<View style={{flex: 1}}>
								<Text>Active contractors:</Text>
								<FlatList
									data={this.state.contractors}
									renderItem={({item}) => (
										<TouchableOpacity
											onPress={() => this.props.navigation.navigate('PayDirect', {nickname: item.DisplayName})}
											style={styles.contractorContainer}
										>
											<Text >{item.DisplayName}</Text>
										</TouchableOpacity>
									)}
									keyExtractor={(item, index) => item.Id}
								/>
							</View>
						) : <Text>No active contractors</Text>
					}
					<NButton
						theme='fb'
						caption='Sign out of QuickBooks'
						type='small'
						onPress={this.handleSignOut}
					/>
				</View>
			);
		}

		return (
			<View style={styles.centeredContainer}>
				<ActivityIndicator size='large' color={nColors.blue} />
			</View>
		);
	}

	renderSignedOut() {
		return (
			<View style={styles.centeredContainer}>
				<NButton
					theme='fb'
					caption='Sign in to QuickBooks!'
					onPress={this.handleSignIn}
				/>
			</View>
		);
	}

	render() {
		return (
			this.state.signedIn 
			? this.renderSignedIn()
			: this.renderSignedOut()
		)
	}
}

const styles = StyleSheet.create({
	centeredContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: nColors.white,
	},
	signedInContainer: {
		flex: 1,
		justifyContent: 'flex-start',
	},
	contractorContainer: {
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: nColors.coolGray,
		textAlign: 'right',
		height: 50,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 5,
		paddingHorizontal: 10,
		paddingVertical: 10,
		marginBottom: 3,
		backgroundColor: nColors.white,
		shadowColor: nColors.black,
		shadowOffset: { width: 0, height: 1} ,
		shadowRadius: 5,
		shadowOpacity: 0.2,
	},
});

