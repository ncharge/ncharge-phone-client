import React from 'react';
import {
	Image,
	Platform,
	ScrollView,
	FlatList,
	StyleSheet,
	Text,
	TouchableOpacity,
	View,
} from 'react-native';

import { MonoText } from '../components/StyledText';
import ContractList from '../components/ContractList';
import TabView from '../components/TabView';
import TxnList from '../components/TxnList';
import nColors from '../common/nColors';

import { Auth, API } from 'aws-amplify';

import {withGlobalContext} from '../common/GlobalContext';

class HomeScreen extends React.Component {
	static navigationOptions = {
		header: null,
	}

	componentDidMount = async () => {
		try {
			await this.props.global.checkIfAdmin();
			if (this.props.global.isAdmin) {
				this.props.global.fetchAdminData();
			}
		} catch (e) {
			console.log("HomeScreen componentDidMount: ", e);
		}
	}

	componentWillUnmount = () => {
		this.fetchAdminDataSub.remove();
	}

	render() {
		const { confirmedContracts, pendingContracts, transactions } = this.props.global;
		return (
			<View style={styles.container}>
				<TabView
					buttonTitles={
						['Confirmed', 'Pending', 'Transactions']
					}
					screens={[
						<ContractList
							key='confirmedContractsList'
							action={() => this.props.navigation.navigate('PayDirect')}
							navigate={
								(nickname, user_id) => {
									this.props.navigation.navigate('ContractDetail', {nickname: nickname, user_id: user_id})
								}
							}
							contracts={confirmedContracts}
							title="Confirmed Contracts"
							refreshData={this.props.global.refreshConfirmedContracts} />,
						<ContractList
							key='pendingContractsList'
							action={() => this.props.navigation.navigate('PayDirect')}
							navigate={
								(nickname, user_id) => {
									this.props.navigation.navigate('ContractDetail', {nickname: nickname, user_id: user_id})
								}
							}
							contracts={pendingContracts}
							title="Pending Contracts"
							refreshData={this.props.global.refreshPendingContracts}
						/>,
						<TxnList
							key="transactionList"
							transactions={transactions}
							title="Transactions"
						/>,
					]}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: nColors.bianca,
	},
});

export default withGlobalContext(HomeScreen);

