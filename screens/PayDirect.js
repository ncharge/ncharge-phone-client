import React from 'react';
import {
	NativeModules,
	Platform,
	ScrollView,
	StyleSheet,
	Text,
	View,
	TextInput,
	TouchableOpacity,
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { CardIOModule, CardIOUtilities } from 'react-native-awesome-card-io';
import { Icon } from 'expo';
import { Formik, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import NButton from '../common/nButton'
import nColors from '../common/nColors'
import Balance from '../components/Balance'
import TabView from '../components/TabView'

import config from '../config';

import { API } from 'aws-amplify';

import {withGlobalContext} from '../common/GlobalContext';

class PayDirect extends React.Component {
	static navigationOptions = {
		title: 'Add Contract',
		headerStyle: {
			backgroundColor: nColors.green
		}
	}

	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			submitErrorMsg: '',
		}
	}

	componentDidMount = () => {
		NativeModules.FinixApi.initTokenizer(
			config.crbApiGateway.URL,
			config.crbApiGateway.APP_ID,
		);
	}

	componentWillMount = () => {
		if(Platform.OS === 'ios') {
			CardIOUtilities.preload();
		}
	}

	scanCard = (values) => {
		CardIOModule
			.scanCard({
				suppressManualEntry: true
			})
			.then(card => {
				values.ccNumber = card.cardNumber;
				values.cvv = card.cvv;
				values.expMonth = card.expiryMonth.toString().padStart(2, '0');
				values.expYear = card.expiryYear.toString();
				this.forceUpdate();
			})
			.catch(() => {
				// the user cancelled
			})
	}

	_handleSubmit = ({nickname, amount, ccNumber, expMonth, expYear}) => {
		this.setState({isLoading: true});

		NativeModules.FinixApi.createToken(
			ccNumber,
			expYear,
			expMonth)
			.then(async (tokenString) => {
				let token = JSON.parse(tokenString);
				const response = await this.payToCard(token.id, nickname, amount);
				console.log('/admin/pay-to-card response:', response);

				this.setState({isLoading: false});

				// TODO: send balance back in response and update directly to avoid extra API call?
				this.props.global.refreshBalance();
				this.props.global.refreshConfirmedContracts();
				this.props.navigation.navigate('Home');
			})
			.catch((e) => {
				console.log('error data:', JSON.stringify(e.response.data));
				this.setState({isLoading: false, submitErrorMsg: e.response.data.errorMessage});
			})
	}

	payToCard = (finixToken, nickname, amount) => {
		return API.post("ncharge", "/admin/pay-to-card", {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: {
				"finix_api_id": 1,
				"card_token": finixToken,
				"nickname": nickname,
				"amount": parseFloat(amount),
			}
		});
	}
			

	renderForm() {
		const validation_schema = Yup.object().shape({
			amount: Yup
				.number()
					.required('Required')
					.test('is-valid-amount', 'Must be an amount between $1-$50,000', (value) => value >= 1 && value <= 50000)
					.typeError('Invalid amount'),
			ccNumber: Yup
				.number()
					.required('Required')
					.typeError('Invalid card number'),
			expMonth: Yup
				.number()
					.required('Required')
					.test('is-valid-month', 'e.g. 02', (value) => value >= 1 && value <= 12)
					.typeError('Invalid month'),
			expYear: Yup
				.number()
					.required('Required')
					.test('is-valid-year', 'e.g. 2020', (value) => value >= new Date().getFullYear())
					.typeError('Invalid year'),
			cvv: Yup
				.number()
					.required('Required')
					.max(999, 'Invalid CVV')
					.typeError('Invalid CVV'),
		});
		const initial_values = {
			nickname: this.props.navigation.getParam('nickname', ''),
			amount: '1000',
			ccNumber: '4895142232120006',
			cvv: '022',
			expMonth: '02',
			expYear: '2020',
		};
		return (
			<Formik
				initialValues={initial_values}
				onSubmit={values => this._handleSubmit(values)}
				validationSchema={validation_schema}
				render={({
					values,
					handleChange,
					handleBlur,
					handleSubmit,
					errors,
					touched,
					resetForm,
					submitCount,
				}) => (
					<ScrollView
						keyboardShouldPersistTaps="never"
						scrollEnabled={false}>
						<NavigationEvents
							onWillFocus={payload => {
								resetForm();
								this.setState({submitErrorMsg: ''});
							}}
						/>
						<TextInput 
							style={[styles.textInput, styles.name]}
							textContentType="nickname"
							placeholder="Name of Contract"
							onChangeText={handleChange('nickname')}
							onBlur={handleBlur('nickname')}
							value={values.nickname}
							returnKeyType={"next"}
							onSubmitEditing={() => { this.amountTextInput.focus(); }}
							blurOnSubmit={false}
						/>
						<Text style={styles.textLabelError}><ErrorMessage name='nickname' /></Text>
						<Text style={styles.textLabel}>Amount</Text>
						<TextInput
							ref={(input) => { this.amountTextInput = input; }}
							style={[styles.textInput, submitCount && errors.amount && touched.amount ? styles.textInputError : null]}
							placeholder="$1000"
							keyboardType="decimal-pad"
							onChangeText={handleChange('amount')}
							onBlur={handleBlur('amount')}
							value={values.amount}
							returnKeyType={"next"}
							onSubmitEditing={() => { this.CCTextInput.focus(); }}
							blurOnSubmit={false}
						/>
						<Text style={styles.textLabelError}>{submitCount ? <ErrorMessage name='amount' /> : null}</Text>
						<Text style={styles.textLabel}>Card Number</Text>
						<View style={{flexDirection: 'row'}}>
							<TextInput
								ref={(input) => { this.CCTextInput = input; }}
								style={[styles.textInput, submitCount && errors.ccNumber && touched.ccNumber ? styles.textInputError : null, {flex: 8}]}
								keyboardType="number-pad"
								textContentType="creditCardNumber"
								placeholder="#### #### #### ####"
								onChangeText={handleChange('ccNumber')}
								onBlur={handleBlur('ccNumber')}
								value={values.ccNumber}
								returnKeyType={"next"}
								onSubmitEditing={() => { this.ExpMonTextInput.focus(); }}
								blurOnSubmit={false}
							/>
							<TouchableOpacity style={styles.scanCardButton} onPress={() => this.scanCard(values)}>
								<Icon.Ionicons name="ios-camera" size={30} />
							</TouchableOpacity>
						</View>
						<Text style={styles.textLabelError}>{submitCount ? <ErrorMessage name='ccNumber' /> : null}</Text>
						<View style={styles.cvv_expiry}>
							<Text style={[styles.textLabel, styles.textLeftHalf]}>Expiration</Text>
							<Text style={[styles.textLabel, styles.textRightHalf]}>CVV</Text>
						</View>
						<View style={styles.cvv_expiry}>
							<View style={styles.expMonth}>
								<TextInput
									ref={(input) => { this.ExpMonTextInput = input; }}
									style={[styles.textInput, styles.textLeftHalf, submitCount && errors.expMonth && touched.expMonth ? styles.textInputError : null]}
									keyboardType="number-pad"
									placeholder="MM"
									onChangeText={handleChange('expMonth')}
									onBlur={handleBlur('expMonth')}
									value={values.expMonth}
									returnKeyType={"next"}
									onSubmitEditing={() => { this.ExpYrTextInput.focus(); }}
									blurOnSubmit={false}
								/>
								<Text style={styles.textLabelError}>{submitCount ? <ErrorMessage name='expMonth' /> : null}</Text>
							</View>
							<View style={styles.expYear}>
								<TextInput
									ref={(input) => { this.ExpYrTextInput = input; }}
									style={[styles.textInput, styles.textRightHalf, styles.textLeftHalf, submitCount && errors.expYear && touched.expYear ? styles.textInputError : null]}
									keyboardType="number-pad"
									placeholder="YYYY"
									onChangeText={handleChange('expYear')}
									onBlur={handleBlur('expYear')}
									value={values.expYear}
									returnKeyType={"next"}
									onSubmitEditing={() => { this.ExpYrTextInput.focus(); }}
									blurOnSubmit={false}
								/>
								<Text style={styles.textLabelError}>{submitCount ? <ErrorMessage name='expYear' /> : null}</Text>
							</View>
							<View style={styles.cvv}>
								<TextInput
									ref={(input) => { this.CVVTextInput = input; }}
									style={[styles.textInput, styles.textRightHalf, submitCount && errors.cvv && touched.cvv ? styles.textInputError : null]}
									keyboardType="number-pad"
									placeholder="###"
									onChangeText={handleChange('cvv')}
									onBlur={handleBlur('cvv')}
									value={values.cvv}
									enablesReturnKeyAutomatically={true}
									onSubmitEditing={this._handleSubmit}
								/>
								<Text style={styles.textLabelError}>{submitCount ? <ErrorMessage name='cvv' /> : null}</Text>
							</View>
						</View>
						<NButton
							style={styles.button}
							theme="green"
							icon={require("../common/img/logo.png")}
							onPress={handleSubmit}
							isLoading={this.state.isLoading}
							caption={values.nickname ? `nCharge $${values.amount} to ${values.nickname}` : 'nCharge'}
						/>
						<View style={{height: 30}}></View>
						<Balance/>
					</ScrollView>
				)}
			/>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<Text style={styles.textLabelError}>{this.state.submitErrorMsg}</Text>
				{this.renderForm()}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	textLabelError: {
		lineHeight: 18,
		height: 18,
		color: 'red',
	},
	textInputError: {
		borderColor: 'red',
	},
	textLabel: {
		marginTop: 2.5,
		fontWeight: 'bold'
	},
	container: {
		flex: 1,
		padding: 30,
		backgroundColor: nColors.white,
	},
	textInput: {
		borderWidth: 0.5,
		borderColor: nColors.coolGray,
		backgroundColor: nColors.white,
		height: 40,
		marginBottom: 2.5,
		paddingLeft: 10,
		borderRadius: 10,
	},
	name: {
		textAlign: 'center',
		backgroundColor: nColors.white,
		borderWidth: 0,
		borderBottomWidth: 1,
		borderBottomColor: nColors.magnesium,
		fontSize: 25,
	},
	cvv_expiry: {
		flexDirection: 'row',
	},
	expMonth: {
		flex: 1,
	},
	expYear: {
		flex: 1,
	},
	cvv: {
		flex: 2,
	},
	textLeftHalf: {
		flex: 1,
		borderTopRightRadius: 0,
		borderBottomRightRadius: 0,
	},
	textRightHalf: {
		flex: 1,
		borderTopLeftRadius: 0,
		borderBottomLeftRadius: 0,
	},
	button: {
		marginTop: 2.5,
	},
	scanCardButton: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
});

export default withGlobalContext(PayDirect);

