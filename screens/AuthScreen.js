import React from 'react';
import {
	TextInput,
	StyleSheet,
	Text,
	Image,
	View,
	ScrollView,
	AlertIOS,
	Dimensions,
	Animated,
	TouchableWithoutFeedback,
	TouchableOpacity,
	KeyboardAvoidingView,
	AsyncStorage,
} from 'react-native';
import {SignUpSwiper} from '../components/SignUpSwiper'
import NButton from '../common/nButton'
import nColors from '../common/nColors'
import { API, Auth } from 'aws-amplify';

import {withGlobalContext} from '../common/GlobalContext';

class AuthScreen extends React.Component {
	static navigationOptions = {
		title: 'Auth',
	};

	constructor (props) {
		super(props);

		this.state = {
			email: '',
			password: '',
			companyname: '',
			ein: '',
			isLoading: false,
			anim: new Animated.Value(0),
			signupOpacity: new Animated.Value(0),
			loginY: new Animated.Value(-120), // this will need to change if relative position of email/password fields change
			newUser: true,
			signup: false,
		}
	}

	componentDidMount = async () => {
		if (__DEV__) {
			this.setState({
				email: 'test+admin@ncharge.io',
				password: 'nCharge1!',
			});
		}

		try {
			if (await Auth.currentSession()) {
				this.props.navigation.navigate('Main');
				console.log("state", this.state);
			}
		} catch (e) {
			console.log(e);
			if (e !== 'No current user') {
				alert(e);
			}
		}
	}

	fadeIn(delay, from = 0) {
		const { anim } = this.state;
		return {
			opacity: anim.interpolate({
				inputRange: [delay, Math.min(delay + 500, 3000)],
				outputRange: [0, 1],
				extrapolate: "clamp"
			}),
			transform: [
				{
					translateY: anim.interpolate({
						inputRange: [delay, Math.min(delay + 500, 3000)],
						outputRange: [from, 0],
						extrapolate: "clamp"
					})
				}
			]
		};
	}

	validateForm() {
		return (this.state.email.length > 0 && this.state.password.length > 0);
	}

	registerAdmin = () => {
		return API.post("ncharge", "/admin",{
			body: { "name": this.state.companyname }
		});
	}

	toggleSignup = () => {
		const signupAnimDuration = 1000, loginAnimDuration = 750;
		if (!this.state.signup) {
			Animated.timing(this.state.signupOpacity, { toValue: 1, duration: signupAnimDuration }).start();
			Animated.timing(this.state.loginY, { toValue: 0, duration: loginAnimDuration }).start();
		} else {
			Animated.timing(this.state.signupOpacity, { toValue: 0, duration: signupAnimDuration }).start();
			Animated.timing(this.state.loginY, { toValue: -120, duration: loginAnimDuration }).start();
		}

		this.setState({signup: !this.state.signup})
	}

	processSignup = async() => {
		this.setState({ isLoading: true });
		try {
			const newUser = await Auth.signUp({
				username: this.state.email,
				password: this.state.password
			});
			await Auth.signIn(this.state.email, this.state.password);
			const admin_response = await this.registerAdmin();
			console.log("registering admin", admin_response);
			this.props.navigation.navigate('PostSignUp');
		} catch (e) {
			//TODO: handle UsernameExistsException
			if (e.code === "UsernameExistsException")
			{
				this.processLogin();
			} else {
				console.log("error", e)
				alert(e.message);
			}
		}
		this.setState({ isLoading: false });
	}

	processLogin = async () => {
		try {
			this.setState({isLoading: true});
			if (!this.validateForm()) {
				throw new Error('Email and/or password fields are empty');
			}

			await Auth.signIn(this.state.email, this.state.password);

			await this.props.global.checkIfAdmin();
			if (this.props.global.isAdmin) {
				this.setState({isLoading: false});
				this.props.navigation.navigate('Main');
			} else {
				throw new Error('You are not an admin');
			}
		} catch (e) {
			alert(e.message);
			this.setState({isLoading: false});
		}
	}

	handleSkipOnboarding = async () => {
		Animated.timing(this.state.anim, { toValue: 3000, duration: 3000 }).start();
		this.setState({newUser: false});
	}

	render() {
		if (this.state.newUser) {
			return (
				<SignUpSwiper skip={this.handleSkipOnboarding}/>
			)
		} else {
			return (
				<KeyboardAvoidingView
					behavior="padding"
					style={styles.container}
				>
					<ScrollView
						keyboardShouldPersistTaps="never"
					>
						<Animated.View 
							style={[styles.header, this.fadeIn(300, 15)]}
						>
							<Image
								style={styles.logo}
								source={require('../common/img/logo.png')}
							/>
						</Animated.View>
						<Animated.View style={[styles.input, this.fadeIn(600, 15)]}>
							<Text style={styles.title}>Welcome to nCharge</Text>
							<Animated.View style={{
								opacity: this.state.signupOpacity,
							}}>
								<TextInput 
									style={styles.textInput} 
									autoCapitalize="none" 
									textContentType="organizationName" 
									placeholder="Company Name" 
									returnKeyType={"next"}
									onChangeText={(companyname) => this.setState({companyname})} value={this.state.companyname} 
									onSubmitEditing={() => { this.einTextInput.focus(); }}
									blurOnSubmit={false}
									editable={this.state.signup}
								/>
								<TextInput 
									ref={(input) => { this.einTextInput = input; }}
									style={styles.textInput} 
									textContentType="organizationName" 
									placeholder="EIN" 
									returnKeyType={"next"}
									onChangeText={(ein) => this.setState({ein})} value={this.state.ein} 
									onSubmitEditing={() => { this.emailTextInput.focus(); }}
									editable={this.state.signup}
								/>
							</Animated.View>
							<Animated.View style={{
								transform: [{ translateY: this.state.loginY }]
							}}>
								<TextInput 
									ref={(input) => { this.emailTextInput = input;}}
									style={styles.textInput} 
									autoCapitalize="none" 
									keyboardType="email-address" 
									textContentType="emailAddress" 
									placeholder="Email" 
									returnKeyType={"next"}
									onChangeText={(email) => this.setState({email})} value={this.state.email} 
									onSubmitEditing={() => { this.passwordTextInput.focus(); }}
									blurOnSubmit={false}
								/>
								<TextInput 
									ref={(input) => { this.passwordTextInput = input; }}
									style={styles.textInput} 
									textContentType="password" 
									secureTextEntry={true} 
									placeholder="Password" 
									enablesReturnKeyAutomatically={true}
									returnKeyType={"done"}
									onChangeText={(password) => this.setState({password})} value={this.state.password} 
									onSubmitEditing={this.state.signup ? this.processSignup : this.processLogin}
								/>
								<NButton 
									caption={this.state.signup ? "Sign up" : "Login"}
									type="small"
									theme="fb"
									onPress={this.state.signup ? this.processSignup : this.processLogin}
									isLoading={this.state.isLoading}
								/>
								<TouchableOpacity 
									onPress={this.toggleSignup}
									style={styles.helplink}
								>
									<Text>
										{this.state.signup ? 'Already have an account?' : "Don't have an account?"} Click here to Sign {this.state.signup ? 'In': 'Up'}!
									</Text>
								</TouchableOpacity>
							</Animated.View>
						</Animated.View>
					</ScrollView>
				</KeyboardAvoidingView>
			);
		}
	}
}

/*Config/Constants*/

const SKIP_BTN_HEIGHT = 24,
WINDOW_WIDTH = Dimensions.get("window").width,
WINDOW_HEIGHT = Dimensions.get("window").height,
VERTICAL_BREAKPOINT = WINDOW_HEIGHT <= 600,
HEADER_HEIGHT = VERTICAL_BREAKPOINT ? 100 : 100,
LOGIN_PADDING_BOTTOM = VERTICAL_BREAKPOINT ? 20 : 33,
CONTENT_PADDING_H = VERTICAL_BREAKPOINT ? 15 : 20;


const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: nColors.white,
	},
	helplink: {
		color: nColors.tangaroa,
		marginTop: 40,
		textAlign: 'center',
		alignItems: 'center',
	},
	logo: {
		marginTop: HEADER_HEIGHT,
		height: 100,
		resizeMode: 'contain',
	},
	input: {
		justifyContent: "space-around",
		paddingHorizontal: CONTENT_PADDING_H,
		paddingBottom: LOGIN_PADDING_BOTTOM,
		paddingTop: 10,
	},
	title: {
		marginTop: 16,
		marginBottom: 30,
		fontSize: 25,
		textAlign: "center",
	},
	textInput: {
		backgroundColor: '#f5f5f5',
		height: 50,
		marginBottom: 10,
		paddingLeft: 10,
		borderRadius: 50,

	},
	header: {
		justifyContent: 'center',
		alignItems: 'center',
	}
});

export default withGlobalContext(AuthScreen);

