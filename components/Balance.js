import React from 'react';
import {
	Text,
} from 'react-native';

import nColors from '../common/nColors';
import {withGlobalContext} from '../common/GlobalContext';

class Balance extends React.Component {
	render() {
		return (<Text>Current Balance: {this.props.global.currentBalance()}¢</Text>)
	}
}
export default withGlobalContext(Balance);
