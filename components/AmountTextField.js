import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	TextInput,
} from 'react-native';
import * as Yup from 'yup';
import { Formik, ErrorMessage } from 'formik';

import nColors from '../common/nColors'

export default class AmountTextField extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			amount: '',
			error:'',
		}

		this.validation_schema = Yup.object().shape({
			amount: Yup
				.number()
					.nullable()
					.required("Required")
					.test('is-valid-amount', 'Must be an amount between $1-$50,000', (value) => value >= 1 && value <= 50000)
					.typeError('Invalid amount')
		});
	}

	handleChange = async (amount) => {
		await this.setState({amount});
		this.setState({error: ''});
		this.validateAmount();
		this.props.disableSubmit(false)
	}
	validateAmount = async () => {
		try{
			await this.validation_schema.validate({amount: this.state.amount})
			this.props.setAmount(this.state.amount);
		} catch(e) {
			this.setState({error: e.message});
			//if validation is handled
			this.props.disableSubmit(true)
			console.log("failed to validate", e);
		}

	}
	renderForm() {
		return (

					<View>
						<TextInput
							ref={(input) => { this.amountTextInput = input; }}
							style={[styles.textInput]}
							placeholder="$1000"
							keyboardType="decimal-pad"
							onChangeText={this.handleChange}
							onBlur={this.validateAmount}
							value={this.state.amount}
							returnKeyType={"done"}
							//onSubmitEditing={() => { this.CCTextInput.focus(); }}
							blurOnSubmit={false}
						/>
						<Text style={styles.textLabelError}>{this.state.error}</Text>
					</View>
		);
	}

	render() {
		return (
			<View style={styles.container}>
				{this.renderForm()}
			</View>
		)
	}
}


const styles = StyleSheet.create({
	textLabelError: {
		lineHeight: 18,
		height: 18,
		color: 'red',
	},
	textInputError: {
		borderColor: 'red',
	},
	textLabel: {
		marginTop: 2.5,
		fontWeight: 'bold'
	},
	container: {
	},
	textInput: {
		borderWidth: 0.5,
		borderColor: nColors.coolGray,
		backgroundColor: nColors.white,
		height: 40,
		marginBottom: 2.5,
		paddingLeft: 10,
		borderRadius: 10,
	}
});
