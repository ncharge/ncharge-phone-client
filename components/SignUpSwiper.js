import React from 'react';
import {
	Image,
} from 'react-native';
import {Foundation, MaterialCommunityIcons} from '@expo/vector-icons'
import nColors from '../common/nColors'
import Onboarding from 'react-native-onboarding-swiper';

export const SignUpSwiper = props => 
	<Onboarding
		onSkip={props.skip}
		onDone={props.skip}
		pages={[
			{
				backgroundColor: '#4567d3',
				image: <Image tintColor='#fff' source={require('../common/img/logo.png')} />,
				title: 'Welcome to nCharge',
				subtitle: 'The Faster and Cheaper Payment Solution',
			},
			{
				backgroundColor: '#3c46be',
				image: <Foundation name="dollar-bill" color={nColors.bianca} size={120} />,
				title: 'Push up to $50,000 directly to Employees and Contractors',
				subtitle: 'in under 30 minutes!',
			},
			{
				backgroundColor: '#c06c84',
				image: <MaterialCommunityIcons name="bank-transfer" color={nColors.bianca} size={120}/>,
				title: 'Save up to 98% on costs',
				subtitle: 'From ACH, Wire Transfer, and Check Processing Fees',
			},
			{
				backgroundColor: '#f67280',
				image: <MaterialCommunityIcons color={nColors.bianca} name="chart-timeline" size={120}/>,
				title: 'Integrate with your existing Payroll solutions',
				subtitle: 'Like Quickbooks or SAP ERP'
			},
		]}
	/>

