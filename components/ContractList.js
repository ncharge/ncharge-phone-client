import React from 'react';
import {
	FlatList,
	StyleSheet,
	Text,
	TouchableOpacity,
	TouchableWithoutFeedback,
	View,
	RefreshControl,
} from 'react-native';

import nColors from '../common/nColors';
import NButton from '../common/nButton';

import { API } from 'aws-amplify';

export default class ContractList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			refreshing: false,
		}
	}

	_onRefresh = () => {
		this.setState({refreshing: true});
		this.props.refreshData().then(() => {
			this.setState({refreshing: false});
		});
	}

	render() {
		const contracts = this.props.contracts;
		if (!contracts || contracts.length === 0) {
			return (
				<View style={styles.contractListContainer}>
					<View style={styles.centerNotice}>
						<Text style={styles.heading}>No {this.props.title ? this.props.title : 'contracts' }</Text>
						<NButton 
							caption="Create a New Contract"
							type="large"
							theme="bordered"
							onPress={this.props.action}
						/>
					</View>
				</View>

			)
		}

		contracts.forEach((contract, i) => {
			contract.key = contract.payee.user_id;
		});
		return (
			<View style={styles.contractListContainer}>
				<FlatList
					data={contracts.reverse()}
					renderItem={({item}) => (
						<TouchableOpacity
							onPress={() => this.props.navigate(item.payee.first_name, item.payee.user_id)}
							style={styles.contractContainer}
						>
							<Text style={styles.contractName}>{item.payee.first_name}</Text>
							<Text style={styles.contractSubtext}>Updated {
								new Date(item.payee.updated_at).toLocaleString()}</Text>
						</TouchableOpacity>
					)}
					refreshControl={
						<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh} />
					}
				/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	contractListContainer: {
		flex:1,
	},
	contractContainer: {
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: nColors.coolGray,
		textAlign: 'right',
		height: 50,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 5,
		paddingHorizontal: 10,
		paddingVertical: 10,
		marginBottom: 3,
		backgroundColor: nColors.white,
		shadowColor: nColors.black,
		shadowOffset: { width: 0, height: 1} ,
		shadowRadius: 5,
		shadowOpacity: 0.2,
	},
	centerNotice: {
		height: 300,
		flexDirection: 'column',
		justifyContent: 'flex-end',
		alignItems: 'center',
	},
	heading: {
		fontWeight: 'bold',
		fontSize: 15,
		marginBottom: 20,
	},
	contractName: {
		fontWeight: 'bold',
		fontSize: 15,
	},
	contractSubtext: {
		fontWeight: '100',
		fontSize: 8,
	},
});	
