import React from 'react';
import {
	FlatList,
	ScrollView,
	StyleSheet,
	Text,
	TouchableOpacity,
	TouchableWithoutFeedback,
	View,
	RefreshControl,
} from 'react-native';

import nColors from '../common/nColors';

export default class TransactionList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			refreshing: false,
		}
	}

	_onRefresh = () => {
		this.setState({refreshing: true});
		this.props.refreshData().then(() => {
			this.setState({refreshing: false});
		});
	}

	formatAmount = (amount) => {
		return (amount/100).toLocaleString("en-US", {style:"currency", currency:"USD"})
		
	}


	render() {
		return (
			//TODO: abstract away Lists into separate component to fill text inputs
			<ScrollView 
				style={styles.contractListContainer}
				keyboardDismissMode='on-drag'
			>
				<FlatList
					data={this.props.transactions.reverse()}
					keyExtractor={(item, index) => item.created_at}
					renderItem={({item}) => (
						<View style={styles.contractContainer}>
							<Text style={styles.contractName}>{this.formatAmount(item.amount)}</Text>
							<Text style={styles.contractSubtext}>{
								new Date(item.created_at).toLocaleString()}</Text>
							<Text style={styles.contractSubtext}>{item.card.last_four}</Text>
						</View>
					)}
				/>
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
	contractListContainer: {
		flex: 1,
	},
	getStartedText: {
		fontSize: 17,
		color: 'rgba(96,100,109, 1)',
		lineHeight: 24,
		textAlign: 'center',
	},
	contractContainer: {
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: nColors.coolGray,
		textAlign: 'right',
		height: 50,
		marginLeft: 10,
		marginRight: 10,
		marginBottom: 5,
		paddingHorizontal: 10,
		paddingVertical: 10,
		marginBottom: 3,
		backgroundColor: nColors.white,
		shadowColor: nColors.black,
		shadowOffset: { width: 0, height: 1} ,
		shadowRadius: 5,
		shadowOpacity: 0.2,
	},
	contractName: {
		fontWeight: 'bold',
		fontSize: 15,
	},
	contractSubtext: {
		fontWeight: '100',
		fontSize: 8,
	},
});	
