import React from 'react';
import {
	Modal,
	SafeAreaView,
	StyleSheet,
	Text,
	TextInput,
	TouchableOpacity,
} from 'react-native';
import nColors from '../common/nColors';
import NButton from '../common/nButton';

import {withGlobalContext} from '../common/GlobalContext';

import { Ionicons } from '@expo/vector-icons';

import { API } from 'aws-amplify';

class ChargeBankAccountModal extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			isLoading: false,
			displayAmount: '$0.00',
			amount: 0,
		}
	}

	chargeBankAccount = async () => {
		this.setState({ isLoading: true });

		try {
			const response = await this.postChargeBankAccount(this.props.account_id, this.state.amount);
			console.log('chargeBankAccount: Received response', response);
			this.props.global.updateBalance(response.balance);
			this.setState({
				displayAmount: '$0.00',
				amount: 0,
			});
			this.props.onDismiss();
		} catch (e) {
			console.log('chargeBankAccount failed', e);
			console.log('chargeBankAccount error response data:', e.response.data);
		}

		this.setState({ isLoading: false });
	}

	postChargeBankAccount = (account_id, amount) => {
		return API.post("ncharge", "/admin/charge-stripe-bank-account", {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			body: {
				"account_id": account_id,
				"amount": amount,
			}
		});
	}

	handleChangeAmount = (displayAmount) => {
		const regex = /\$(\d*)\.(\d*)/;
		if (!regex.test(displayAmount)) {
			return;
		}
		const amount = parseInt(''.concat(...regex.exec(displayAmount).slice(1)));
		const paddedAmount = amount.toString().padStart(3, '0');
		const newDisplayAmount = `$${paddedAmount.slice(0, paddedAmount.length - 2)}.${paddedAmount.slice(-2)}`;

		this.setState({
			displayAmount: newDisplayAmount,
			displayAmountLength: newDisplayAmount.length,
			amount,
		});
	}

	render() {
		return (
			<Modal
				animationType='slide'
				transparent={false}
				visible={this.props.visible}
			>
				<SafeAreaView style={styles.container}>
					<TouchableOpacity
						style={styles.closeModalBtn}
						onPress={this.props.onDismiss}
					>
						<Ionicons name='ios-close' size={40} color={nColors.coolGray}/>
					</TouchableOpacity>
					<Text style={styles.amountLabel}>Amount</Text>
					<TextInput
						defaultValue='$0.00'
						style={styles.amountInput}
						placeholder="$1000"
						keyboardType="decimal-pad"
						onChangeText={this.handleChangeAmount}
						value={this.state.displayAmount}
						returnKeyType={"send"}
					/>
					<NButton
						style={styles.button}
						theme={parseInt(this.state.amount) > 0 ? 'fb' : 'disabled'}
						type="small"
						onPress={this.chargeBankAccount}
						isLoading={this.state.isLoading}
						caption={`Add ${this.state.displayAmount} to balance`}
					/>
				</SafeAreaView>
			</Modal>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		marginHorizontal: 20,
	},
	closeModalBtn: {
		marginRight: 10,
		alignSelf: 'flex-end',
	},
	amountLabel: {
		marginTop: 2.5,
		fontWeight: 'bold'
	},
	amountInput: {
		borderWidth: 0.5,
		borderColor: nColors.coolGray,
		backgroundColor: nColors.white,
		height: 40,
		marginBottom: 2.5,
		paddingLeft: 10,
		borderRadius: 10,
	},
});

export default withGlobalContext(ChargeBankAccountModal);

