import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Modal,
	ActivityIndicator
} from 'react-native';
import NButton from '../common/nButton'
import nColors from '../common/nColors'

export default class LoadingModal extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			visible: this.props.visible,
			message: this.props.message,
		}
	}
	static defaultProps = {
		visible: false,
		cancelable: false,
		message: '',
		animation: 'none',
		color: 'white',
		size: 'large',
		overlayColor: 'rgba(0, 0, 0, 0.25)'
	}

	static getDerivedStateFromProps(props, state) {
		const newState = {};
		if (state.visible !== props.visible) newState.visible = props.visible;
		if (state.textContent !== props.textContent)
			newState.textContent = props.textContent;
		return newState;
	}

	close = () => {
		this.setState({visible: false});
	}

	_handleOnRequestClose() {
		if(this.props.cancelable) {
			this.close();
		}
	}

	_renderDefaultContent() {
		return (
			<View style={styles.background}>
				{this.props.customIndicator ? (this.props.customIndicator):(
					<ActivityIndicator
						color={this.props.color}
						size={this.props.size}
						style={[styles.activityIndicator, {...this.props.indicatorStyle}]}
					/>
				)}
				<View style={[styles.messageContainer, {...this.props.indicatorStyle}]}>
					<Text style={[styles.message, this.props.textStyle]}>
						{this.state.message}
					</Text>
				</View>
			</View>
		);
	}

	_renderSpinner() {
		if(!this.state.visible) return null;

		const spinner = (
			<View style={[styles.container, { backgroundColor: this.props.overlayColor }]}
				key={`spinner_${Date.now()}`}
			>
				{this._renderDefaultContent()}
			</View>
		);

		return (
			<Modal
				animationType={this.props.animation}
				onRequestClose={() => this._handleOnRequestClose}
				supportedOrientations={['portrait']}
				transparent
				visible={this.state.visible}
			>
				{spinner}
			</Modal>
		);
	}

	render() {
		return this._renderSpinner();
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'transparent',
		position: 'absolute',
		top: 0,
		bottom: 0,
		left: 0,
		right: 0
	},
	background: {
		position: 'absolute',
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		justifyContent: 'center',
		alignItems: 'center'
	},
	messageContainer: {
		flex: 1,
		top: 0,
		bottom: 0,
		left: 0,
		right: 0,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute'
	},
	message: {
		top: 80,
		height: 50,
		fontSize: 20,
		fontWeight: 'bold'
	},
	activityIndicator: {
		flex: 1
	}
});
