import 'react-native';
import React from 'react';
import ContractList from '../ContractList';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
	const contracts_mock = [];
	const tree = renderer.create(<ContractList contracts={contracts_mock}>Snapshot test!</ContractList>).toJSON();

	expect(tree).toMatchSnapshot();
});

