import React from 'react';
import {
	Dimensions,
	FlatList,
	StyleSheet,
	Text,
	TouchableOpacity,
	Image,
	View,
	StatusBar,
} from 'react-native';

import nColors from '../common/nColors';

const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} = Dimensions.get('window');
const STATUS_BAR_HEIGHT = 44;
const HEADER_HEIGHT = 21 + STATUS_BAR_HEIGHT;
const IOS_ITEM_TEXT_SIZE = SCREEN_WIDTH < 375 ? 10 : 13;

const HEADER_FONT_COLOR = nColors.tangaroa;

class TabNav extends React.Component {
	constructor(props) {
		super(props);
	}

	handlePress = (index) => {
		this.props.onSelectTab(index);
	}

	render() {
		return (
			<View style={styles.tabNavContainer}>
				{ this.props.titles && this.props.titles.map((title, index) => 
							<TouchableOpacity
								style={[styles.buttonContainer,
									this.props.selectedIndex === index
										? styles.buttonSelected
										: {}]}
								key={title}
								onPress={() => {
									this.handlePress(index)
								}}
							>
								<Text style={styles.buttonText}>{title}</Text>
							</TouchableOpacity>
					)
				}
			</View>

		);
	}
}

class TabView extends React.Component {
	constructor (props) {
		super(props);

		this.state = {
			selectedTabIndex: 0,
		}
	}

	componentWillUpdate = () => {
	}
	handleSelectTab = (index) => {
		this.setState({selectedTabIndex: index});
		this.scrollview.scrollToIndex({index: index});
	}

	handleMomentumScrollEnd = (e) => {
		const contentOffsetX = e.nativeEvent.contentOffset.x;
		this.setState({selectedTabIndex: contentOffsetX / SCREEN_WIDTH});
	}

	render() {
		return (
			<View style={styles.tabViewContainer}>
				<View style={styles.headerContainer}>
					{
						this.props.headerTitle && <Text style={styles.headerTitleText}>{this.props.headerTitle}</Text>
					}
					{
						!this.props.headerTitle && <Image style={styles.logo} source={require("../common/img/logo.png")}/> 
					}
					{this.props.buttonTitles && <TabNav
						titles={this.props.buttonTitles}
						onSelectTab={this.handleSelectTab}
						selectedIndex={this.state.selectedTabIndex}
					/>
					}
				</View>
				<FlatList
					ref={(scrollview) => this.scrollview = scrollview}
					data={this.props.screens}
					renderItem={({item, index}) => (
						<View style={styles.tabContentContainer}>
							{item}
						</View>
					)}
					horizontal={true}
					contentContainerStyle={styles.flatListContentContainer}
					pagingEnabled={true}
					showsHorizontalScrollIndicator={false}
					bounces={false}
					onMomentumScrollEnd={this.handleMomentumScrollEnd}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	tabNavContainer: {
		height: 50,
		flexDirection: 'row',
		justifyContent: 'center',
	},
	buttonContainer: {
		height: 30,
		margin: 10,
		paddingHorizontal: 10,
		paddingVertical: 5,
		borderStyle: 'solid',
		borderRadius: 20,
		borderWidth: 1,
		borderColor: 'transparent',
	},
	buttonSelected: {
		borderColor: HEADER_FONT_COLOR,
	},
	buttonText: {
		fontSize: 14,
		color: HEADER_FONT_COLOR,
	},
	tabViewContainer: {
		flex: 1,
		backgroundColor: nColors.white,
	},
	headerContainer: {
		backgroundColor: nColors.green,
		paddingTop: STATUS_BAR_HEIGHT,
		color: HEADER_FONT_COLOR,
		justifyContent: 'center',
		alignItems: 'center',
	},
	headerTitleText: {
		fontSize: 17,
		fontWeight: '600',
		textAlign: 'center',
		color: HEADER_FONT_COLOR,
		padding: 20,
	},
	flatListContentContainer: {
		paddingTop: 30,
	},
	tabContentContainer: {
		width: SCREEN_WIDTH
	},
	logo: {
		tintColor: HEADER_FONT_COLOR,
		resizeMode: 'contain',
		height: 50,
	}
});

export default TabView;

