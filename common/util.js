import { MAX_AMOUNT } from '../config'

export const validateAmount = (amount_data) => {
	const amount = parseFloat(amount_data)
	if ((amount <= MAX_AMOUNT && amount > 0) || amount_data === '') {
		return true;
	} else {
		return false;
	}
}

export const handleChangeAmount = (displayAmount) => {
	const regex = /\$(\d*)\.(\d*)/;
	if (!regex.test(displayAmount)) {
		return;
	}
	const amount = ''.concat(...regex.exec(displayAmount).slice(1));
	const paddedAmount = parseInt(amount).toString().padStart(3, '0');
	const newDisplayAmount = `$${paddedAmount.slice(0, paddedAmount.length - 2)}.${paddedAmount.slice(-2)}`;
}
