import React from 'react';

import { API } from 'aws-amplify';

const GlobalContext = React.createContext({});

export default class GlobalContextProvider extends React.Component {
	state = {
		//HomeScreen
		isAdmin: false,
		confirmedContracts: [],
		pendingContracts: [],
		transactions: [],
		//Payment Details
		balance: 0,
		cards: [],
		plaidAccount: null,
	}

	logInfo = (...args) => {
		console.log(`${this.constructor.displayName}:`, ...args);
	}

	fetchAdminData = () => {
		try {
			this.refreshConfirmedContracts();
			this.refreshPendingContracts();
			this.refreshTransactions();
			this.refreshBalance();
		} catch (e) {
			console.log("Caught:", e);
			console.log("Error response data:", e.response.data);
		}
	}

	checkIfAdmin = async () => {
		const isAdmin = await API.get("ncharge", "/is_admin");
		console.log('isAdmin', isAdmin);
		this.setState({isAdmin});
		return isAdmin;
	}

	refreshConfirmedContracts = async () => {
		const oldLength = this.state.confirmedContracts.length;
		const confirmedContracts = await API.get("ncharge", "/admin/payees", {
			queryStringParameters: {
				"admin_confirmed": true,
				"payee_confirmed": true,
			}
		});
		this.setState({confirmedContracts});
		this.logInfo(`Refreshed confirmed contracts, ${confirmedContracts.length - oldLength} new items`);
		return confirmedContracts;
	}
	refreshPendingContracts = async () => {
		const oldLength = this.state.pendingContracts.length;
		const pendingContracts = await API.get("ncharge", "/admin/payees", {
			queryStringParameters: {
				"admin_confirmed": false,
				"payee_confirmed": true,
			}
		});
		this.setState({pendingContracts});
		this.logInfo(`Refreshed pending contracts, ${pendingContracts.length - oldLength} new items`);
		return pendingContracts;
	}
	refreshTransactions = async () => {
		const oldLength = this.state.transactions.length;
		const transactions = await API.get("ncharge", "/admin/transactions");
		this.setState({transactions});
		this.logInfo(`Refreshed transactions, ${transactions.length - oldLength} new items`);
		return transactions;
	}

	currentBalance = () => this.state.balance
	refreshBalance = async () => {
		const balance = await API.get("ncharge", "/admin/balance");
		this.updateBalance(balance);
		return balance;
	}
	updateBalance = (balance) => {
		this.logInfo(`Updating current balance ${this.state.balance} to new balance ${balance}`);
		this.setState({balance});
	}

	cardData = () => this.state.cards
	updateCardData = (data) => {
		this.logInfo("Setting card data:", data);
		this.setState({cards: data});
	}

	refreshPlaidAccount = async () => {
		try {
			const response = await this.fetchPlaidAccount();
			this.logInfo('Received response', response);
			if (response != null && response.account != null) {
				this.logInfo('Existing account found for current admin, setting state');
				this.updatePlaidAccount(
					response.account.account_id,
					response.account.name,
					response.account.mask,
				);
			}
		} catch (e) {
			this.logInfo('getPlaidSelectedAccount failed', e.response.data.errorMessage);
		}
	}

	fetchPlaidAccount = () => {
		return API.get("ncharge", "/admin/plaid-selected-account", {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
		});
	}

	updatePlaidAccount = (id, name, mask) => {
		this.setState({
			plaidAccount: {
				id,
				name,
				mask,
			}
		});
	};

	clearContext = () => {
		this.setState({
			isAdmin: false,
			confirmedContracts: [],
			pendingContracts: [],
			transactions: [],
			balance: 0,
			cards: [],
			plaidAccount: null,
		});
	}

	render () {
		return (
			<GlobalContext.Provider
				value={{
					...this.state,
					logInfo: this.logInfo,
					checkIfAdmin: this.checkIfAdmin,
					fetchAdminData: this.fetchAdminData,
					refreshConfirmedContracts: this.refreshConfirmedContracts,
					refreshPendingContracts: this.refreshPendingContracts,
					refreshTransactions: this.refreshTransactions,
					currentBalance: this.currentBalance,
					updateBalance: this.updateBalance,
					refreshBalance: this.refreshBalance,
					cardData: this.cardData,
					updateCardData: this.updateCardData,
					refreshPlaidAccount: this.refreshPlaidAccount,
					updatePlaidAccount: this.updatePlaidAccount,
					clearContext: this.clearContext,
				}}
			>
				{this.props.children}
			</GlobalContext.Provider>
		)
	}
}

// create the consumer as higher order component
export const withGlobalContext = ChildComponent => (
	class GlobalContextConsumer extends React.Component {
		static navigationOptions = ChildComponent.navigationOptions;
		render() {
			return (
				<GlobalContext.Consumer>
					{
						context => <ChildComponent {...this.props} global={context}  />
					}
				</GlobalContext.Consumer>
			);
		}
	}
);

