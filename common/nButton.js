"use strict";

import React from "react";
import nColors from "./nColors";
import { ActivityIndicator, Text, View, StyleSheet, TouchableOpacity, Image, Alert } from "react-native";

/* constants ================================================================ */

const BUTTON_HEIGHT = 52,
  BUTTON_HEIGHT_SM = 32,
  BUTTON_HEIGHT_SET = 42;

/* <nButton />
============================================================================= */

export default class NButton extends React.Component {
  props: {
    theme:
      | "pink"
      | "blue"
      | "yellow"
      | "fb"
      | "white"
      | "bordered"
      | "bordered-pink"
	  | "settings"
      | "disabled",
    type: "default" | "round" | "small",
    opacity: number,
    icon?: number,
    caption?: string,
    style?: any,
    fontSize?: number,
    // buttonColor?: string;
    // contentColor?: string;
	onPress: () => mixed,
	isLoading?: boolean,
  };

  static defaultProps = {
    opacity: 1,
    theme: "pink"
  };

  static height = BUTTON_HEIGHT;

  render() {
    const { icon, fontSize, opacity, isLoading } = this.props;
    const caption = this.props.caption && this.props.caption.toUpperCase();
    const { buttonTheme, iconTheme, captionTheme } = this.getTheme();
    const { containerType, buttonType, iconType, captionType } = this.getType();

    let iconImage;
    if (icon) {
      iconImage = (
        <Image source={icon} style={[styles.icon, iconTheme, iconType]} />
      );
    }

	if (isLoading) {
		iconImage = (<ActivityIndicator color={iconTheme.tintColor} style={[styles.icon, iconTheme, iconType]}/>)
	}

    let fontSizeOverride;
    if (fontSize) {
      fontSizeOverride = { fontSize };
    }

    const content = (
      <View style={[styles.button, buttonTheme, buttonType, { opacity }]}>
        {iconImage}
        <Text
          style={[styles.caption, captionTheme, captionType, fontSizeOverride]}
        >
          {caption}
        </Text>
      </View>
    );

    if (this.props.onPress) {
      return (
        <TouchableOpacity
          accessibilityTraits="button"
		  onPress={this.props.theme === 'disabled' || this.props.isLoading ? () => {}: this.props.onPress }
          activeOpacity={0.5}
          style={[styles.container, containerType, this.props.style]}
        >
          {content}
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={[styles.container, containerType, this.props.style]}>
          {content}
        </View>
      );
    }
  }

  getTheme() {
    const { theme } = this.props;
    let buttonTheme, iconTheme, captionTheme;
    if (theme === "yellow") {
      buttonTheme = { backgroundColor: nColors.yellow };
      iconTheme = { tintColor: nColors.pink };
      captionTheme = { color: nColors.pink };
    } else if (theme === "blue") {
      buttonTheme = { backgroundColor: nColors.blue };
      iconTheme = { tintColor: nColors.white };
      captionTheme = { color: nColors.white };
    } else if (theme === "green") {
      buttonTheme = { backgroundColor: nColors.green };
      iconTheme = { tintColor: nColors.tangaroa };
      captionTheme = { color: nColors.tangaroa };
    } else if (theme === "fb") {
      buttonTheme = { backgroundColor: nColors.facebookBlue };
      iconTheme = { color: nColors.white, tintColor: nColors.white };
      captionTheme = { color: nColors.white };
    } else if (theme === "white") {
      buttonTheme = { backgroundColor: nColors.white };
      iconTheme = { tintColor: nColors.pink };
      captionTheme = { color: nColors.pink };
    } else if (theme === "bordered") {
      buttonTheme = {
        backgroundColor: "transparent",
        borderWidth: 1,
        borderColor: nColors.tangaroa
      };
      iconTheme = { tintColor: nColors.tangaroa };
      captionTheme = { color: nColors.tangaroa };
    } else if (theme === "bordered-pink") {
      buttonTheme = {
        backgroundColor: "transparent",
        borderWidth: 1,
        borderColor: nColors.pink
      };
      iconTheme = { tintColor: nColors.pink };
      captionTheme = { color: nColors.pink };
    } else if (theme === "maps") {
      buttonTheme = {
        backgroundColor: "transparent",
        borderWidth: 1,
        borderColor: nColors.purple
      };
      iconTheme = { tintColor: nColors.tangaroa };
      captionTheme = { color: nColors.tangaroa };
    } else if (theme === "mapsInactive") {
      buttonTheme = {
        backgroundColor: "transparent",
        borderWidth: 1,
        borderColor: "transparent"
      };
      iconTheme = { tintColor: nColors.tangaroa };
      captionTheme = { color: nColors.tangaroa };
    } else if (theme === "disabled") {
      buttonTheme = { backgroundColor: nColors.blueBayoux };
      iconTheme = { tintColor: nColors.white, opacity: 0.5 };
      captionTheme = { color: nColors.white, opacity: 0.5 };
    } else if (theme === "transparent") {
      buttonTheme = { backgroundColor: "transparent" };
      iconTheme = { tintColor: nColors.tangaroa };
      captionTheme = { color: nColors.tangaroa };
	} else if (theme === "settings") {
		buttonTheme = { 
			backgroundColor: nColors.white,
			width: '100%',
			borderWidth: 1,
			borderRadius: 0,
			borderColor: '#eee',
		};
      iconTheme = { tintColor: nColors.tangaroa };
		captionTheme = { 
			color: nColors.tangaroa,
		};
	} else {
      // pink/white is default
      buttonTheme = { backgroundColor: nColors.pink };
      iconTheme = { tintColor: "white" };
      captionTheme = { color: "white" };
    }

    return { buttonTheme, iconTheme, captionTheme };
  }

  getType() {
    const { type } = this.props;
    let containerType, buttonType, iconType, captionType;

    if (type === "round") {
      buttonType = { width: BUTTON_HEIGHT, paddingHorizontal: 0 };
      iconType = { marginRight: 0 };
      captionType = { fontSize: 13 };
    } else if (type === "small") {
      containerType = { height: BUTTON_HEIGHT_SM };
      buttonType = { paddingHorizontal: 15 };
      iconType = { marginRight: 0 };
      captionType = { fontSize: 13 };
	} else if (type === "settings") {
		containerType = { height: BUTTON_HEIGHT_SET };
		buttonType={ justifyContent: "flex-start"}
		captionType = { textAlign: "left"};
	} else {
      // defaults
    }

    return { containerType, buttonType, iconType, captionType };
  }
}

/* StyleSheet
============================================================================= */

const styles = StyleSheet.create({
  container: {
    height: BUTTON_HEIGHT
  },
  button: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 30,
    borderRadius: BUTTON_HEIGHT / 2
  },
  buttonRound: {
    width: BUTTON_HEIGHT,
    paddingHorizontal: 0
  },
  icon: {
    marginRight: 12,
	resizeMode: 'contain',
	width: 30,
    height: 30,
  },
  caption: {
    fontSize: 15,
    textAlign: "center"
  }
});

